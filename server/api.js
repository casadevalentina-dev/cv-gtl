var Api = new Restivus({
	useDefaultAuth: false,
	prettyJson: true
});

GetTheLook = new Mongo.Collection('getthelook');

Api.addRoute('categories', {authRequired: false, enableCors: true}, {
	get: function () {
		result =  Agile.getCategories();
		this.response.write(result);
		this.done();
	}
});

Api.addCollection(GetTheLook, {
	excludedEndpoints: ['put'],
	routeOptions: {
		authRequired: false
	}
});

Api.addRoute('posts/:id', {authRequired: false}, {
	get: function () {
		return GetTheLook.find({post: this.urlParams.id}).fetch();
	}
});

Api.addRoute('products', {authRequired: false}, {
	get: function () {
		let query = this.queryParams;
		let result = Agile.getProducts(query);

		return result;
	}
});