Agile = {};

Agile._url = 'http://www.agileecommerce.com.br/app/api/';

Agile._getCategories = function (cb) {
	HTTP.get(Agile._url + 'ConsultarCategorias?codcategoriapai=1&order=ordem', {npmRequestOptions: {gzip: true},
		headers: {
			'Token': 'YMkEcfzPISlspMHD3ZqFZ24fCdPJij'
		}}, function (error, result) {
			cb(result.content);
		});
}

Agile._getProducts = function (query, cb) {

	let string = '';

	if (query.category)
		string += 'codcategoria=' + query.category + '&';

	if (query.name)
		string += 'busca=' + query.name + '&';

	HTTP.get(Agile._url + 'ConsultarProdutos?' + string + 'limit=10', {npmRequestOptions: {gzip: true}, 
		headers: {
			'Token': 'YMkEcfzPISlspMHD3ZqFZ24fCdPJij'
		}},
		function (error, result) {
			cb(result.content);
		});
}

Agile.getCategories = Meteor.wrapAsync(Agile._getCategories);
Agile.getProducts = Meteor.wrapAsync(Agile._getProducts);